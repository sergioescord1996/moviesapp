//
//  DetailView.swift
//  MoviesApp
//
//  Created by Sergio Escalante Ordonez on 17/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit
import RxSwift

class DetailView: UIViewController {
    
    @IBOutlet private weak var titleHeader: UILabel!
    @IBOutlet private weak var filmImage: UIImageView!
    @IBOutlet private weak var movieDescription: UILabel!
    @IBOutlet private weak var releaseDate: UILabel!
    @IBOutlet private weak var originalTitle: UILabel!
    @IBOutlet private weak var rate: UILabel!
    
    var movieID: String?
    
    private var detailViewModel = DetailViewModel()
    private var router = DetailRouter()
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailViewModel.bind(view: self, router: router)
        
        getDataAndShowDetailMovie()
    }
    
    func showMovieData(movie: MovieDetail) {
        DispatchQueue.main.async {
            self.titleHeader.text = movie.title
            self.filmImage.imageFromServerURL(with: Constants.URL.urlImages + movie.image)
            self.movieDescription.text = movie.description
            self.rate.text = String(movie.rate)
            self.releaseDate.text = movie.releaseDate
            self.originalTitle.text = movie.originalTitle
        }
    }
    
    func getDataAndShowDetailMovie() {
        
        guard let movieID = movieID else { return }
        
        return detailViewModel.getMovieData(movieID: movieID).subscribe(onNext: { (movieDetail) in
            self.showMovieData(movie: movieDetail)
        }, onError: { (error) in
            print("Error fetching movie detail: \(error)")
        }).disposed(by: disposeBag)
    }
}
