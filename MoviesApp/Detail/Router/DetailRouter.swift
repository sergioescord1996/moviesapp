//
//  DetailRouter.swift
//  MoviesApp
//
//  Created by Sergio Escalante Ordonez on 17/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

class DetailRouter {
    
    var viewController: UIViewController {
        return getViewController()
    }
    
    private var sourceView: UIViewController?
    
    var movieID: String?
    
    init(movieID: String? = "") {
        self.movieID = movieID
    }
    
    private func getViewController() -> UIViewController {
        let viewController = DetailView(nibName: "DetailView", bundle: Bundle.main)
        viewController.movieID = movieID
        return viewController
    }
    
    func setSourceView(_ sourceView: UIViewController?) {
        guard let view = sourceView else { fatalError("Unkwon error") }
        self.sourceView = view
    }
}
