//
//  MovieDetail.swift
//  MoviesApp
//
//  Created by Sergio Escalante Ordonez on 17/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

struct MovieDetail: Codable {
    let title: String
    let image: String
    let description: String
    let originalTitle: String
    let homepage: String
    let releaseDate: String
    let rate: Double
    
    enum CodingKeys: String, CodingKey {
        case title
        case image = "poster_path"
        case description = "overview"
        case releaseDate = "release_date"
        case originalTitle = "original_title"
        case rate = "vote_average"
        case homepage
    }
}
