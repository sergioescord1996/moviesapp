//
//  NetworkManager.swift
//  MoviesApp
//
//  Created by Sergio Escalante Ordonez on 17/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation
import RxSwift

class NetworkManager {
    
    func getPopularMovies() -> Observable<[Movie]> {
        
        return Observable.create { observer in
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: Constants.URL.main + Constants.Endpoints.urlListPopularMovies + Constants.apiKey)!)
            
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil, let response = response as? HTTPURLResponse else { return }
                
                if response.statusCode == 200 {
                    do {
                        let decoder = JSONDecoder()
                        let movies = try decoder.decode(Movies.self, from: data)
                        
                        observer.onNext(movies.listOfMovies)
                    } catch {
                        observer.onError(error)
                        print("Unkown error: \(error.localizedDescription)")
                    }
                } else if response.statusCode == 401 {
                    print("Error 401")
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create {
                session.finishTasksAndInvalidate()
            }
        }
    }
    
    func getDetailMovies(movieID: String) -> Observable<MovieDetail> {
        
        return Observable.create { observer in
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: Constants.URL.main + Constants.Endpoints.urlDetailMovie + "/\(movieID)" + Constants.apiKey)!)
            
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil, let response = response as? HTTPURLResponse else { return }
                
                if response.statusCode == 200 {
                    do {
                        let decoder = JSONDecoder()
                        let movie = try decoder.decode(MovieDetail.self, from: data)
                        
                        observer.onNext(movie)
                    } catch {
                        observer.onError(error)
                        print("Unkown error: \(error.localizedDescription)")
                    }
                } else if response.statusCode == 401 {
                    print("Error 401")
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create {
                session.finishTasksAndInvalidate()
            }
        }
    }
}
