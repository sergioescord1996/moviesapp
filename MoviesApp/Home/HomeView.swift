//
//  HomeView.swift
//  MoviesApp
//
//  Created by Sergio Escalante Ordonez on 17/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeView: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activity: UIActivityIndicatorView!
    
    private var router = HomeRouter()
    private var viewModel = HomeViewModel()
    private var disposeBag = DisposeBag()
    private var movies = [Movie]()
    private var filteredMovies = [Movie]()
    
    lazy var searchController: UISearchController = ({
        
        let controller = UISearchController(searchResultsController: nil)
        controller.hidesNavigationBarDuringPresentation = true
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = .black
        controller.searchBar.backgroundColor = .clear
        controller.searchBar.placeholder = "Buscar una película"
        
        return controller
    })()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "The Movies App"
        
        viewModel.bind(view: self, router: router)
        
        configureTableView()
        
        manageSearchBarController()
        
        getData()
    }
    
    private func configureTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
    }
    
    private func getData() {
        return viewModel.getListMoviesData()
            .subscribe(on: MainScheduler.instance)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { movies in
                self.movies = movies
                self.reloadTableView()
            }, onError: { error in
                print("Unkown error: \(error.localizedDescription)")
            }, onCompleted: {}).disposed(by: disposeBag)
    }
    
    private func reloadTableView() {
        DispatchQueue.main.async {
            self.activity.stopAnimating()
            self.activity.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    private func manageSearchBarController() {
        let searchBar = searchController.searchBar
        searchController.delegate = self
        tableView.tableHeaderView = searchBar
//        tableView.contentOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
        
        searchBar.rx.text
            .orEmpty
            .distinctUntilChanged().subscribe(onNext: { (result) in
                self.filteredMovies = self.movies.filter({ (movie) -> Bool in
                    self.reloadTableView()
                    return movie.title.contains(result)
                })
                self.tableView.reloadData()
                return
            })
            .disposed(by: disposeBag)
    }
}

extension HomeView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if searchController.isActive && searchController.searchBar.text != "" {
            viewModel.makeDetailView(movieID: String(filteredMovies[indexPath.row].movieID))
        } else {
            viewModel.makeDetailView(movieID: String(movies[indexPath.row].movieID))
        }
    }
}

extension HomeView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return self.filteredMovies.count
        }
        
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? MovieCell else { return UITableViewCell() }
        
        //guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MovieCell.self), for: indexPath) as? MovieCell else { return UITableViewCell() }
        
        if searchController.isActive && searchController.searchBar.text != "" {
            cell.movieImage.imageFromServerURL(with: "\(Constants.URL.urlImages + self.filteredMovies[indexPath.row].image)")
            cell.movieTitle.text = filteredMovies[indexPath.row].originalTitle
            cell.movieDescription.text = filteredMovies[indexPath.row].sinopsis
        } else {
            cell.movieImage.imageFromServerURL(with: "\(Constants.URL.urlImages + self.movies[indexPath.row].image)")
            cell.movieTitle.text = movies[indexPath.row].originalTitle
            cell.movieDescription.text = movies[indexPath.row].sinopsis
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

extension HomeView: UISearchControllerDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.isActive = false
        reloadTableView()
    }
}
