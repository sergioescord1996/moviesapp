//
//  Constants.swift
//  MoviesApp
//
//  Created by Sergio Escalante Ordonez on 17/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

struct Constants {
    
    static let apiKey = "?api_key=<API_KEY>"
    
    struct URL {
        static let main = "https://api.themoviedb.org/3"
        static let urlImages = "https://image.tmdb.org/t/p/w200"
    }
    
    struct Endpoints {
        static let urlListPopularMovies = "/movie/popular"
        static let urlDetailMovie = "/movie"
    }
}
